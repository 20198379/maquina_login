﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace login_c
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int idusuario;
        public class lusuario
        {
           public int IDUsuario { get; set; }
           public string nombre { get; set; }
           public string apellido { get; set; }
           public int telefono { get; set; }
           public string usuario { get; set; }
           public string pass { get; set; }
           public string departamento { get; set; }
        }
        public class CONEXIONMAESTRA
        {

            public static string conexion = @"Data source=DESKTOP-OPL3O34; Initial Catalog =master; Integrated Security= true";
            public static SqlConnection conectar = new SqlConnection(conexion);
            public static void abrir()
            {
                if (conectar.State == ConnectionState.Closed)
                {
                    conectar.Open();
                }
            }
            public static void cerrar()
            {
                if (conectar.State == ConnectionState.Open)
                {
                    conectar.Close();
                }
            }
        }
        public class UserDao : CONEXIONMAESTRA
        {
            public void Login(lusuario par, ref int ID)
            {
                try
                {
                    CONEXIONMAESTRA.abrir();
                    SqlCommand cmd = new SqlCommand("validar_usuario", CONEXIONMAESTRA.conectar);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pass", par.pass);
                    cmd.Parameters.AddWithValue("@usuario", par.usuario);
                    ID = Convert.ToInt32(cmd.ExecuteScalar());
                    //MessageBox.Show("BIENVENIDO");
                    
                }
                catch (Exception ex)
                {
                    ID = 0;
                    MessageBox.Show(ex.Message);
                    
                }
                finally
                {
                    CONEXIONMAESTRA.cerrar();
                }
            }
        }
        private void btnEntrar_Click(object sender, EventArgs e)
        {
            UserDao ud = new UserDao();
            lusuario par = new lusuario();
            par.pass = txtcontra.Text;
            par.usuario = txtusuario.Text;
            ud.Login(par, ref idusuario);
            if (idusuario > 0)
            {
                
                Bienve BN = new Bienve();
                BN.ShowDialog();
            }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

